import 'package:flutter/material.dart';
import 'package:trio_gym/Configuration/sizeConfig.dart';

class RoundedButtonLogin extends StatefulWidget {
  final Function onTap;
  final Widget title;
  final Color color;
  final Color borderColor;

  const RoundedButtonLogin({
    Key key,
    this.onTap,
    this.title,
    this.color,
    this.borderColor,
  }) : super(key: key);

  @override
  _RoundedButtonLoginState createState() => _RoundedButtonLoginState();
}

class _RoundedButtonLoginState extends State<RoundedButtonLogin> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        height: SizeConfig.height * 0.06,
        decoration: BoxDecoration(
            border: Border.all(color: widget.borderColor),
            borderRadius: BorderRadius.circular(40),
            color: widget.color),
        child: Center(
          child: widget.title,
        ),
      ),
    );
  }
}
