import 'package:flutter/painting.dart';

class Palette {
  static const Color iconColor = Color(0xFFF07138);
  static const Color activeColor = Color(0xFF09126C);
  static const Color textColor1 = Color(0XFF5f100a);
  static const Color textColor2 = Color(0XFF5f100a);
  static const Color facebookColor = Color(0xFF3B5999);
  static const Color googleColor = Color(0xFFDE4B39);
  static const Color backgroundColor = Color(0xFFECF3F9);
  static const Color mainTeilColor = Color(0xff20D5B2);
}
