import 'package:flutter/material.dart';
import 'package:trio_gym/Configuration/sizeConfig.dart';

class UserProfileScreen extends StatefulWidget {
  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {
  Color _iconColor = Colors.black;
  Color _textColor = Colors.black;
  List<Color> _actionContainerColor = [
    Color.fromRGBO(249, 249, 249, 1),
    Color.fromRGBO(241, 241, 241, 1),
    Color.fromRGBO(233, 233, 233, 1),
    Color.fromRGBO(222, 222, 222, 1),
  ];
  Color _borderContainer = Color.fromRGBO(32, 178, 170, 1);
  bool colorSwitched = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Gym X',
          style: TextStyle(color: Colors.black),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios_outlined),
          color: Colors.black,
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15)),
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [Color(0xff20D5B2), Colors.teal])),
              child: Container(
                width: double.infinity,
                height: SizeConfig.height * 0.25,
                child: Center(
                  child: Stack(
                    children: [
                      Column(
                        children: [
                          SizedBox(height: SizeConfig.height * 0.06),
                          Container(
                            height: SizeConfig.height * 0.14,
                            child: Card(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 5.0),
                              clipBehavior: Clip.antiAlias,
                              color: Colors.white,
                              elevation: 5.0,
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8.0, vertical: 22.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        children: [
                                          Text(
                                            "Info.",
                                            style: TextStyle(
                                              color: Colors.redAccent,
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          SizedBox(height: 5.0),
                                          Text(
                                            "28.5K",
                                            style: TextStyle(
                                              fontSize: 20.0,
                                              color: Colors.pinkAccent,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                    Spacer(),
                                    Expanded(
                                      child: Column(
                                        children: <Widget>[
                                          Text(
                                            "Data",
                                            style: TextStyle(
                                              color: Colors.redAccent,
                                              fontSize: 22.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          SizedBox(height: 5.0),
                                          Text(
                                            "1300",
                                            style: TextStyle(
                                              fontSize: 20.0,
                                              color: Colors.pinkAccent,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Positioned(
                        top: SizeConfig.height * 0.04,
                        left: SizeConfig.width * 0.32,
                        child: Column(
                          children: [
                            CircleAvatar(
                              backgroundImage: NetworkImage(
                                "https://www.history.ox.ac.uk/sites/default/files/styles/person_profile_photo/public/history/images/person/unknown-person-icon-4_0.jpg?itok=o2SPU-Ax",
                              ),
                              radius: SizeConfig.width * 0.12,
                            ),
                            SizedBox(height: SizeConfig.height * 0.04),
                            Text(
                              "Nedal Al-zaben",
                              style: TextStyle(
                                fontSize: 22.0,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )),
          Spacer(),
          Container(
            height: SizeConfig.height * 0.6,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: _borderContainer,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15),
                topRight: Radius.circular(15),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15),
                      topRight: Radius.circular(15)),
                  gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    stops: [0.2, 0.4, 0.6, 0.8],
                    colors: _actionContainerColor,
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Column(
                        children: <Widget>[
                          Text(
                            '790',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: _textColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 30),
                          ),
                          Text(
                            'Available Cash',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: _iconColor, fontSize: 16),
                          ),
                        ],
                      ),
                    ),
                    Table(
                      border: TableBorder.symmetric(
                          // inside: BorderSide(
                          //     color: Colors.grey,
                          //     style: BorderStyle.solid,
                          //     width: 0.5),
                          ),
                      children: [
                        TableRow(children: [
                          _actionList(
                              'assets/profile/ic_send.png', 'Send Money'),
                          _actionList('assets/profile/ic_money.png', 'Request'),
                        ]),
                        TableRow(children: [
                          _actionList(
                              'assets/profile/ic_transact.png', 'Transactions'),
                          _actionList(
                              'assets/profile/ic_reward.png', 'Reward Points'),
                        ])
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _actionList(String iconPath, String desc) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            iconPath,
            fit: BoxFit.contain,
            height: 45.0,
            width: 45.0,
            color: _iconColor,
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            desc,
            style: TextStyle(color: _iconColor),
          )
        ],
      ),
    );
  }
}
