import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trio_gym/Configuration/sizeConfig.dart';
import 'package:expansion_tile_card/expansion_tile_card.dart';
import 'package:trio_gym/View/Screens/GymProfile/GymProfile.dart';
import 'package:trio_gym/View/Screens/MyGymDetails/MyGymDetails.dart';
import 'package:trio_gym/View/Screens/UserProfile/UserProfileScreen.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:expandable_bottom_bar/expandable_bottom_bar.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ExpansionTileCardState> cardA = new GlobalKey();
  bool folded = true;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Theme.of(context).canvasColor,

      // backgroundColor: Color.,
      // extendBody: true,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'All gyms',
          style: TextStyle(color: Colors.black),
        ),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => UserProfileScreen()));
            },
            icon: Icon(
              Icons.person_sharp,
              color: Colors.black,
            ),
          ),
        ],
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Colors.black,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: GestureDetector(
        // Set onVerticalDrag event to drag handlers of controller for swipe effect
        onVerticalDragUpdate: DefaultBottomBarController.of(context).onDrag,
        onVerticalDragEnd: DefaultBottomBarController.of(context).onDragEnd,
        child: FloatingActionButton.extended(
          label: Text("My gyms"),
          elevation: 2,
          backgroundColor: Colors.deepOrange,
          foregroundColor: Colors.white,
          onPressed: () => DefaultBottomBarController.of(context).swap(),
        ),
      ),

      bottomNavigationBar: BottomExpandableAppBar(
        expandedHeight: SizeConfig.height * 0.6,
        horizontalMargin: 2,
        shape: AutomaticNotchedShape(
          RoundedRectangleBorder(),
          StadiumBorder(
            side: BorderSide(),
          ),
        ),
        expandedBackColor: Theme.of(context).backgroundColor,
        expandedBody: Padding(
          padding: EdgeInsets.all(SizeConfig.width * 0.03),
          child: Column(
            children: [
              SizedBox(height: SizeConfig.height * 0.05),
              Container(
                decoration: BoxDecoration(),
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => MyGymDetails()));
                  },
                  child: Row(
                    children: [
                      CircleAvatar(
                        backgroundColor: Colors.deepOrange,
                        radius: 30,
                      ),
                      SizedBox(width: SizeConfig.width * 0.05),
                      Text(
                        'Gym name',
                        style: TextStyle(
                          fontSize: SizeConfig.width * 0.05,
                        ),
                      ),
                      Spacer(),
                      IconButton(
                          icon: Icon(Icons.arrow_forward_ios),
                          onPressed: () {}),
                      SizedBox(height: SizeConfig.height * 0.05),
                    ],
                  ),
                ),
              ),
              SizedBox(height: SizeConfig.height * 0.05),
              Container(
                decoration: BoxDecoration(),
                child: Row(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.deepOrange,
                      radius: 30,
                    ),
                    SizedBox(width: SizeConfig.width * 0.05),
                    Text(
                      'Gym name',
                      style: TextStyle(
                        fontSize: SizeConfig.width * 0.05,
                      ),
                    ),
                    Spacer(),
                    IconButton(
                        icon: Icon(Icons.arrow_forward_ios), onPressed: () {}),
                    SizedBox(height: SizeConfig.height * 0.05),
                  ],
                ),
              ),
            ],
          ),
        ),
        bottomAppBarBody: Padding(
            padding: EdgeInsets.all(SizeConfig.width * 0.02),
            child: Container()),
      ),
      body: Padding(
        padding: EdgeInsets.all(SizeConfig.width * 0.05),
        child: Container(
          child: Stack(
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: !folded ? SizeConfig.height * 0.08 : 0),
                child: Expanded(
                  child: Container(
                    child: ListView.builder(
                      itemCount: 10,
                      itemBuilder: (context, i) {
                        return Column(
                          children: [
                            ExpansionTileCard(
                              // key: cardA,
                              leading: CircleAvatar(child: Text('A')),
                              title: Text('Gym name'),
                              subtitle: Text('Short description'),
                              children: <Widget>[
                                Divider(
                                  thickness: 1.0,
                                  height: 1.0,
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 16.0,
                                      vertical: 8.0,
                                    ),
                                    child: Text(
                                      "This is about the gym information and all quick data\n"
                                      "here is the data about subscription and payment or what ever",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2
                                          .copyWith(fontSize: 16),
                                    ),
                                  ),
                                ),
                                ButtonBar(
                                  alignment: MainAxisAlignment.spaceAround,
                                  buttonHeight: 52.0,
                                  buttonMinWidth: 90.0,
                                  children: <Widget>[
                                    TextButton(
                                      // style: flatButtonStyle,
                                      onPressed: () {
                                        _launchMaps();
                                      },
                                      child: Column(
                                        children: <Widget>[
                                          Icon(CupertinoIcons.map),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 2.0),
                                          ),
                                          Text('Map'),
                                        ],
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {},
                                      child: Column(
                                        children: <Widget>[
                                          Icon(CupertinoIcons
                                              .phone_arrow_up_right),
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 2.0),
                                          ),
                                          Text('Call'),
                                        ],
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder:
                                                    (BuildContext context) =>
                                                        GymProfile()));
                                      },
                                      child: Column(
                                        children: <Widget>[
                                          Icon(CupertinoIcons.profile_circled),
                                          Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 2.0),
                                          ),
                                          Text('Profile'),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Divider(
                              thickness: 1,
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ),
              Positioned(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    AnimatedContainer(
                      duration: Duration(milliseconds: 400),
                      width: folded
                          ? SizeConfig.width * 0.14
                          : SizeConfig.width * 0.88,
                      height: SizeConfig.width * 0.14,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(32),
                        color: !folded
                            ? Color(0xff20D5B2)
                            : Color(0xff20D5B2).withOpacity(0.5),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(
                                  left: SizeConfig.width * 0.06),
                              child: !folded
                                  ? TextField(
                                      decoration: InputDecoration(
                                        hintText: 'Search',
                                        hintStyle:
                                            TextStyle(color: Colors.white),
                                        border: InputBorder.none,
                                      ),
                                    )
                                  : null,
                            ),
                          ),
                          AnimatedContainer(
                            duration: Duration(milliseconds: 400),
                            child: Material(
                              type: MaterialType.transparency,
                              child: InkWell(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(folded ? 32 : 0),
                                  topRight: Radius.circular(folded ? 32 : 0),
                                  bottomLeft: Radius.circular(folded ? 32 : 0),
                                  bottomRight: Radius.circular(folded ? 32 : 0),
                                ),
                                child: Padding(
                                  padding:
                                      EdgeInsets.all(SizeConfig.width * 0.04),
                                  child: Icon(
                                    folded ? Icons.search : Icons.close,
                                    color: Colors.white,
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    folded = !folded;
                                  });
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _launchMaps() async {
    String googleUrl = 'comgooglemaps://?center=${31.945368},${35.928371}';
    String appleUrl = 'https://maps.apple.com/?sll=${31.945368},${35.928371}';
    if (await canLaunch("comgooglemaps://")) {
      print('launching com googleUrl');
      await launch(googleUrl);
    } else if (await canLaunch(appleUrl)) {
      print('launching apple url');
      await launch(appleUrl);
    } else {
      throw 'Could not launch url';
    }
  }
}
