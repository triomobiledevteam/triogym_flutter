import 'package:flutter/material.dart';
import 'package:trio_gym/Configuration/sizeConfig.dart';

import 'Payment.dart';

class PaymentDetailsScreen extends StatefulWidget {
  const PaymentDetailsScreen({Key key}) : super(key: key);

  @override
  _PaymentDetailsScreenState createState() => _PaymentDetailsScreenState();
}

class _PaymentDetailsScreenState extends State<PaymentDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFe52165),
        title: Text('Gym name payments'),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: SizeConfig.height * 0.02),
            Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: SizeConfig.width * 0.05),
              child: Row(
                children: [
                  Container(
                    child: Text(
                      'Payments information',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig.width * 0.045),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.01),
            Row(
              children: [
                Container(
                  height: SizeConfig.height * 0.05,
                  width: SizeConfig.width * 0.8,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                    color: Color(0xFFe52165).withOpacity(0.5),
                  ),
                  child: Row(
                    children: [
                      SizedBox(width: SizeConfig.width * 0.05),
                      Container(
                        width: SizeConfig.width * 0.4,
                        child: Text(
                          'Total',
                          style: TextStyle(fontSize: SizeConfig.width * 0.045),
                        ),
                      ),
                      Spacer(),
                      Container(
                        width: SizeConfig.width * 0.3,
                        child: Text(
                          ': 800 JOD',
                          style: TextStyle(fontSize: SizeConfig.width * 0.045),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Row(
              children: [
                Container(
                  height: SizeConfig.height * 0.05,
                  width: SizeConfig.width * 0.8,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                    color: Color(0xFFa2d5c6).withOpacity(0.5),
                  ),
                  child: Row(
                    children: [
                      SizedBox(width: SizeConfig.width * 0.05),
                      Container(
                        width: SizeConfig.width * 0.4,
                        child: Text(
                          'Discount',
                          style: TextStyle(fontSize: SizeConfig.width * 0.045),
                        ),
                      ),
                      Spacer(),
                      Container(
                        width: SizeConfig.width * 0.3,
                        child: Text(
                          ': 60 JOD',
                          style: TextStyle(fontSize: SizeConfig.width * 0.045),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Row(
              children: [
                Container(
                  height: SizeConfig.height * 0.05,
                  width: SizeConfig.width * 0.8,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                    color: Color(0xFFe52165).withOpacity(0.5),
                  ),
                  child: Row(
                    children: [
                      SizedBox(width: SizeConfig.width * 0.05),
                      Container(
                        width: SizeConfig.width * 0.4,
                        child: Text(
                          'Tax',
                          style: TextStyle(fontSize: SizeConfig.width * 0.045),
                        ),
                      ),
                      Spacer(),
                      Container(
                        width: SizeConfig.width * 0.3,
                        child: Text(
                          ': 128 JOD',
                          style: TextStyle(fontSize: SizeConfig.width * 0.045),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Row(
              children: [
                Container(
                  height: SizeConfig.height * 0.05,
                  width: SizeConfig.width * 0.8,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                    color: Color(0xFFa2d5c6).withOpacity(0.5),
                  ),
                  child: Row(
                    children: [
                      SizedBox(width: SizeConfig.width * 0.05),
                      Container(
                        width: SizeConfig.width * 0.4,
                        child: Text(
                          'Net after discount',
                          style: TextStyle(fontSize: SizeConfig.width * 0.045),
                        ),
                      ),
                      Spacer(),
                      Container(
                        width: SizeConfig.width * 0.3,
                        child: Text(
                          ': 740 JOD',
                          style: TextStyle(fontSize: SizeConfig.width * 0.045),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Row(
              children: [
                Container(
                  height: SizeConfig.height * 0.05,
                  width: SizeConfig.width * 0.8,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    ),
                    color: Color(0xFFe52165).withOpacity(0.5),
                  ),
                  child: Row(
                    children: [
                      SizedBox(width: SizeConfig.width * 0.05),
                      Container(
                        width: SizeConfig.width * 0.4,
                        child: Text(
                          'Net after tax',
                          style: TextStyle(fontSize: SizeConfig.width * 0.045),
                        ),
                      ),
                      Spacer(),
                      Container(
                        width: SizeConfig.width * 0.3,
                        child: Text(
                          ': 868 JOD',
                          style: TextStyle(fontSize: SizeConfig.width * 0.045),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Divider(thickness: 2),
            SizedBox(height: SizeConfig.height * 0.02),
            Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: SizeConfig.width * 0.05),
              child: Row(
                children: [
                  Container(
                    child: Text(
                      'Payments log',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig.width * 0.045),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.01),
            Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: SizeConfig.width * 0.05),
              child: Container(
                child: Table(
                  columnWidths: {
                    0: FixedColumnWidth(
                        SizeConfig.width * 0.12), // fixed to 100 width
                    // 1: FlexColumnWidth(),
                    // 2: FixedColumnWidth(100.0), //fixed to 100 width
                    // 3: FixedColumnWidth(100.0), //fixed to 100 width
                  },
                  // border: TableBorder.all(color: Colors.black),
                  children: [
                    TableRow(children: [
                      Container(
                        color: Color(0xFFe52165),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text(
                            'No.',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                      ),
                      Container(
                        color: Color(0xFFe52165),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text(
                            'Date',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                      ),
                      Container(
                        color: Color(0xFFe52165),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text(
                            'Paid',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                      ),
                      Container(
                        color: Color(0xFFe52165),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text(
                            'Paid for',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ]),
                    TableRow(children: [
                      Container(
                        color: Color(0xFFa2d5c6).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('1'),
                        ),
                      ),
                      Container(
                        color: Color(0xFFa2d5c6).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('10-03-2021'),
                        ),
                      ),
                      Container(
                        color: Color(0xFFa2d5c6).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('220'),
                        ),
                      ),
                      Container(
                        color: Color(0xFFa2d5c6).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('ABB gym'),
                        ),
                      ),
                    ]),
                    TableRow(children: [
                      Container(
                        color: Color(0xFFe52165).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('3'),
                        ),
                      ),
                      Container(
                        color: Color(0xFFe52165).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('16-05-2021'),
                        ),
                      ),
                      Container(
                        color: Color(0xFFe52165).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('80'),
                        ),
                      ),
                      Container(
                        color: Color(0xFFe52165).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('Spa'),
                        ),
                      ),
                    ]),
                    TableRow(children: [
                      Container(
                        color: Color(0xFFa2d5c6).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('1'),
                        ),
                      ),
                      Container(
                        color: Color(0xFFa2d5c6).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('15-05-2021'),
                        ),
                      ),
                      Container(
                        color: Color(0xFFa2d5c6).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('115'),
                        ),
                      ),
                      Container(
                        color: Color(0xFFa2d5c6).withOpacity(0.5),
                        child: Padding(
                          padding: EdgeInsets.all(SizeConfig.width * 0.02),
                          child: Text('My gym2'),
                        ),
                      ),
                    ]),
                    TableRow(children: [
                      Padding(
                        padding: EdgeInsets.all(SizeConfig.width * 0.02),
                        child: Text(' '),
                      ),
                      Padding(
                        padding: EdgeInsets.all(SizeConfig.width * 0.02),
                        child: Text('Total'),
                      ),
                      Padding(
                        padding: EdgeInsets.all(SizeConfig.width * 0.02),
                        child: Text('415'),
                      ),
                      Padding(
                        padding: EdgeInsets.all(SizeConfig.width * 0.02),
                        child: Text(' '),
                      ),
                    ]),
                  ],
                ),
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: SizeConfig.width * 0.05),
              child: Container(
                child: Text(
                  'You paid 415 remaining 453',
                  style: TextStyle(fontSize: SizeConfig.width * 0.045),
                ),
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: SizeConfig.width * 0.05),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => Payment()));
                },
                child: Container(
                  width: SizeConfig.width * 0.4,
                  height: SizeConfig.height * 0.05,
                  decoration: BoxDecoration(
                      color: Color(0xFFa2d5c6),
                      borderRadius: BorderRadius.circular(10)),
                  child: Center(
                    child: Text(
                      'New payment',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig.width * 0.045),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
