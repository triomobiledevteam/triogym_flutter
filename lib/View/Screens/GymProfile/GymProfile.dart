import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trio_gym/Configuration/sizeConfig.dart';
import 'package:trio_gym/View/Screens/Login/LoginScreen.dart';

class GymProfile extends StatefulWidget {
  @override
  _GymProfileState createState() => _GymProfileState();
}

class _GymProfileState extends State<GymProfile> {
  int _currentIndex = 0;
  PageController _pageController;
  final List<List<String>> products = [
    ['assets/gym/Packages/Packages1.jpeg', 'Spacial Package No 1', '100 \$'],
    ['assets/gym/Packages/Packages2.jpeg', 'Package No 2', '120 \$'],
    ['assets/gym/Packages/Packages3.jpg', 'Premium Package ', '80 \$']
  ];

  int currentIndex = 0;

  void _next() {
    setState(() {
      if (currentIndex < products.length - 1) {
        currentIndex++;
      } else {
        currentIndex = currentIndex;
      }
    });
  }

  void _preve() {
    setState(() {
      if (currentIndex > 0) {
        currentIndex--;
      } else {
        currentIndex = 0;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavyBar(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        selectedIndex: _currentIndex,
        showElevation: true,
        // use this to remove appBar's elevation
        onItemSelected: (index) => setState(() {
          _currentIndex = index;
          _pageController.animateToPage(index,
              duration: Duration(milliseconds: 300), curve: Curves.ease);
        }),
        items: [
          BottomNavyBarItem(
            icon: Icon(CupertinoIcons.cube_box),
            title: Text('Packages'),
            activeColor: Colors.red,
          ),
          BottomNavyBarItem(
              icon: Icon(Icons.subscriptions_outlined),
              title: Text('Subscribe'),
              activeColor: Colors.purpleAccent),
          BottomNavyBarItem(
              icon: Icon(Icons.sports_handball),
              title: Text('Training'),
              activeColor: Colors.pink),
        ],
      ),
      backgroundColor: Color(0xff20D5B2),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        title: Text('Gym profile'),
      ),
      body: Column(
        children: [
          // Spacer(),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(SizeConfig.width * 0.05),
              child: Container(
                child: Column(
                  children: [
                    Spacer(),
                    Row(
                      children: [
                        Spacer(),
                        Icon(Icons.email_outlined, color: Colors.white),
                        Text(
                          ' lnedal44@gmail.com',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(width: SizeConfig.width * 0.05),
                        Icon(Icons.mobile_friendly_outlined,
                            color: Colors.white),
                        Text(
                          ' 0799580802',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: SizeConfig.height * 0.7,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.0),
                topRight: Radius.circular(20.0),
              ),
            ),
            child: PageView(
              controller: _pageController,
              onPageChanged: (index) {
                setState(() => _currentIndex = index);
              },
              children: [
                Padding(
                  padding: EdgeInsets.all(SizeConfig.width * 0.05),
                  child: Column(
                    children: [
                      Expanded(
                        child: Container(
                          child: ListView(
                            children: [
                              _buildFoodItem(
                                  'assets/gym/gym1.jpeg', 'Offer 1', '\$24.00'),
                              _buildFoodItem(
                                  'assets/gym/gym2.jpeg', 'Offer 2', '\$22.00'),
                              _buildFoodItem(
                                  'assets/gym/gym3.png', 'Offer 3', '\$26.00'),
                              _buildFoodItem(
                                  'assets/gym/gym5.png', 'Offer 4', '\$24.00'),
                              _buildFoodItem(
                                  'assets/gym/gym5.png', 'Offer 4', '\$24.00'),
                              _buildFoodItem(
                                  'assets/gym/gym5.png', 'Offer 4', '\$24.00'),
                              _buildFoodItem(
                                  'assets/gym/gym5.png', 'Offer 4', '\$24.00'),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        onHorizontalDragEnd: (DragEndDetails details) {
                          if (details.velocity.pixelsPerSecond.dx > 0) {
                            _preve();
                          } else if (details.velocity.pixelsPerSecond.dx < 0) {
                            _next();
                          }
                        },
                        child: Container(
                          width: double.infinity,
                          height: SizeConfig.height * 0.53,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(20.0),
                                topRight: Radius.circular(20.0),
                              ),
                              image: DecorationImage(
                                  image: AssetImage(products[currentIndex][0]),
                                  fit: BoxFit.cover)),
                          child: Container(
                            decoration: BoxDecoration(
                                gradient: LinearGradient(
                                    begin: Alignment.bottomRight,
                                    colors: [
                                  Colors.grey[700].withOpacity(.9),
                                  Colors.grey.withOpacity(.0),
                                ])),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                  width: 90,
                                  margin: EdgeInsets.only(bottom: 60),
                                  child: Row(
                                    children: _buildIndicator(),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: SizeConfig.height * 0.3,
                          width: SizeConfig.width,
                          padding: EdgeInsets.symmetric(
                              horizontal: SizeConfig.width * 0.03,
                              vertical: SizeConfig.width * 0.01),
                          color: Colors.white,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(height: 5),
                              Text(
                                products[currentIndex][1],
                                style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: <Widget>[
                                  Text(
                                    products[currentIndex][2],
                                    style: TextStyle(
                                        color: Colors.yellow[700],
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                  SizedBox(width: 10),
                                  Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.star,
                                        size: 18,
                                        color: Colors.yellow[700],
                                      ),
                                      Icon(
                                        Icons.star,
                                        size: 18,
                                        color: Colors.yellow[700],
                                      ),
                                      Icon(
                                        Icons.star,
                                        size: 18,
                                        color: Colors.yellow[700],
                                      ),
                                      Icon(
                                        Icons.star,
                                        size: 18,
                                        color: Colors.yellow[700],
                                      ),
                                      Icon(
                                        Icons.star_half,
                                        size: 18,
                                        color: Colors.yellow[700],
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                        "(4.2/70 reviews)",
                                        style: TextStyle(
                                            color: Colors.grey, fontSize: 12),
                                      )
                                    ],
                                  )
                                ],
                              ),
                              SizedBox(height: 15),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.width * 0.05),
                        child: InkWell(
                          onTap: () {
                            // LoginSignupScreen
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => LoginSignupScreen()));
                          },
                          child: Container(
                            height: SizeConfig.height * 0.05,
                            decoration: BoxDecoration(
                                color: Colors.yellow[700],
                                borderRadius: BorderRadius.circular(8)),
                            child: Center(
                              child: Text(
                                "Subscribe",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: SizeConfig.width * 0.05),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: SizeConfig.width * 0.02),
                    ],
                  ),
                ),
                Center(child: Text('TEST')),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _indicator(bool isActive) {
    return Expanded(
      child: Container(
        height: 10,
        margin: EdgeInsets.only(right: 5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: isActive ? Colors.grey[800] : Colors.white),
      ),
    );
  }

  List<Widget> _buildIndicator() {
    List<Widget> indicators = [];
    for (int i = 0; i < products.length; i++) {
      if (currentIndex == i) {
        indicators.add(_indicator(true));
      } else {
        indicators.add(_indicator(false));
      }
    }

    return indicators;
  }

  Widget _buildFoodItem(String imgPath, String foodName, String price) {
    return Padding(
      padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
      child: InkWell(
        onTap: () {
          // Navigator.of(context).push(MaterialPageRoute(
          //     builder: (context) => DetailsPage(heroTag: imgPath, foodName: foodName, foodPrice: price)
          // ));
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Row(
                children: [
                  Image(
                    image: AssetImage(imgPath),
                    fit: BoxFit.cover,
                    height: 75.0,
                    width: 75.0,
                  ),
                  SizedBox(width: 10.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(foodName,
                          style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 17.0,
                              fontWeight: FontWeight.bold)),
                      Text(price,
                          style: TextStyle(
                              fontFamily: 'Montserrat',
                              fontSize: 15.0,
                              color: Colors.grey))
                    ],
                  )
                ],
              ),
            ),
            IconButton(
                icon: Icon(Icons.add), color: Colors.black, onPressed: () {})
          ],
        ),
      ),
    );
  }
}
