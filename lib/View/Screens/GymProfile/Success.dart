import 'package:flutter/material.dart';
import 'package:trio_gym/Configuration/sizeConfig.dart';

class Success extends StatefulWidget {
  final int index;

  const Success({Key key, this.index}) : super(key: key);

  @override
  _SuccessState createState() => _SuccessState();
}

class _SuccessState extends State<Success> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            'assets/done.png',
            width: SizeConfig.width * 0.5,
          ),
          Padding(
            padding: EdgeInsets.all(SizeConfig.width * 0.05),
            child: Text(
              'Your payment was done successfully',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: SizeConfig.width * 0.05,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              if (widget.index == 0) {
                Navigator.pop(context);
                Navigator.pop(context);
                Navigator.pop(context);
              } else {
                Navigator.pop(context);
                Navigator.pop(context);
              }
            },
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: SizeConfig.width * 0.1),
              child: Container(
                height: SizeConfig.height * 0.05,
                // width: SizeConfig.width * 0.9,
                decoration: BoxDecoration(
                    color: Color(0xFFe52165),
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                  child: Text(
                    'Done',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.width * 0.045),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
