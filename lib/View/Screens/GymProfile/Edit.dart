import 'package:flutter/material.dart';
import 'package:trio_gym/Configuration/sizeConfig.dart';

class EditSubscription extends StatefulWidget {
  const EditSubscription({Key key}) : super(key: key);

  @override
  _EditSubscriptionState createState() => _EditSubscriptionState();
}

class _EditSubscriptionState extends State<EditSubscription> {
  String dropdownValue1;
  String dropdownValue2;
  String dropdownValue3;
  String dropdownValue4;
  String dropdownValue5;
  String dropdownValue6;
  String dropdownValue7;
  String dropdownValue8;
  String dropdownValue9;

  var subJorType = [
    'Freeze',
    'Cancel',
    'Renew',
    'Transfer to another customer',
    'Upgrade to another subscription',
    'Unfreeze'
  ];
  var paymentTerm = ['30 days', '60 days', '90 days', '120 days'];
  var gymLocation = ['Amman', 'Az zarqa', 'Irbed', 'Salt'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFa2d5c6),
        title: Text(
          'Gym name edit subscription',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(SizeConfig.width * 0.05),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Subscription journal type',
              style: TextStyle(fontSize: SizeConfig.width * 0.04),
            ),
            SizedBox(height: SizeConfig.height * 0.01),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(.1),
                  borderRadius: BorderRadius.circular(10)),
              child: DropdownButton<String>(
                value: dropdownValue1,
                isExpanded: true,
                style: TextStyle(
                    color: Colors.grey, fontSize: SizeConfig.width * 0.03),
                hint: Text(
                  'Action on your subscription',
                  style: TextStyle(color: Colors.black),
                ),
                onChanged: (value) => setState(() => dropdownValue1 = value),
                items: subJorType.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,
                        style: TextStyle(inherit: false, color: Colors.red)),
                  );
                }).toList(),
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Text(
              'Payment term',
              style: TextStyle(fontSize: SizeConfig.width * 0.04),
            ),
            SizedBox(height: SizeConfig.height * 0.01),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(.1),
                  borderRadius: BorderRadius.circular(10)),
              child: DropdownButton<String>(
                value: dropdownValue2,
                isExpanded: true,
                style: TextStyle(
                    color: Colors.grey, fontSize: SizeConfig.width * 0.03),
                hint: Text(
                  'Select period',
                  style: TextStyle(color: Colors.black),
                ),
                onChanged: (value) => setState(() => dropdownValue2 = value),
                items:
                    paymentTerm.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,
                        style: TextStyle(inherit: false, color: Colors.red)),
                  );
                }).toList(),
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Text(
              'Gym location',
              style: TextStyle(fontSize: SizeConfig.width * 0.04),
            ),
            SizedBox(height: SizeConfig.height * 0.01),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(.1),
                  borderRadius: BorderRadius.circular(10)),
              child: DropdownButton<String>(
                value: dropdownValue3,
                isExpanded: true,
                style: TextStyle(
                    color: Colors.grey, fontSize: SizeConfig.width * 0.03),
                hint: Text(
                  'Select gym branch',
                  style: TextStyle(color: Colors.black),
                ),
                onChanged: (value) => setState(() => dropdownValue3 = value),
                items:
                    gymLocation.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,
                        style: TextStyle(inherit: false, color: Colors.red)),
                  );
                }).toList(),
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Text(
              'Subscription journal type',
              style: TextStyle(fontSize: SizeConfig.width * 0.04),
            ),
            SizedBox(height: SizeConfig.height * 0.01),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(.1),
                  borderRadius: BorderRadius.circular(10)),
              child: DropdownButton<String>(
                value: dropdownValue4,
                isExpanded: true,
                style: TextStyle(
                    color: Colors.grey, fontSize: SizeConfig.width * 0.03),
                hint: Text(
                  'Action on your subscription',
                  style: TextStyle(color: Colors.black),
                ),
                onChanged: (value) => setState(() => dropdownValue4 = value),
                items: subJorType.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,
                        style: TextStyle(inherit: false, color: Colors.red)),
                  );
                }).toList(),
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Text(
              'Subscription journal type',
              style: TextStyle(fontSize: SizeConfig.width * 0.04),
            ),
            SizedBox(height: SizeConfig.height * 0.01),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(.1),
                  borderRadius: BorderRadius.circular(10)),
              child: DropdownButton<String>(
                value: dropdownValue5,
                isExpanded: true,
                style: TextStyle(
                    color: Colors.grey, fontSize: SizeConfig.width * 0.03),
                hint: Text(
                  'Action on your subscription',
                  style: TextStyle(color: Colors.black),
                ),
                onChanged: (value) => setState(() => dropdownValue5 = value),
                items: subJorType.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,
                        style: TextStyle(inherit: false, color: Colors.red)),
                  );
                }).toList(),
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Text(
              'Subscription journal type',
              style: TextStyle(fontSize: SizeConfig.width * 0.04),
            ),
            SizedBox(height: SizeConfig.height * 0.01),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(.1),
                  borderRadius: BorderRadius.circular(10)),
              child: DropdownButton<String>(
                value: dropdownValue6,
                isExpanded: true,
                style: TextStyle(
                    color: Colors.grey, fontSize: SizeConfig.width * 0.03),
                hint: Text(
                  'Action on your subscription',
                  style: TextStyle(color: Colors.black),
                ),
                onChanged: (value) => setState(() => dropdownValue6 = value),
                items: subJorType.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value,
                        style: TextStyle(inherit: false, color: Colors.red)),
                  );
                }).toList(),
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.02),
            Spacer(),
            InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                height: SizeConfig.height * 0.05,
                width: SizeConfig.width * 0.9,
                decoration: BoxDecoration(
                    color: Color(0xFFe52165),
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                  child: Text(
                    'Send request',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.width * 0.045),
                  ),
                ),
              ),
            ),
            SizedBox(height: SizeConfig.height * 0.02),
          ],
        ),
      ),
    );
  }
}
