import 'package:credit_card/credit_card_form.dart';
import 'package:credit_card/credit_card_model.dart';
import 'package:credit_card/credit_card_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:trio_gym/Configuration/sizeConfig.dart';
import 'package:trio_gym/View/Screens/GymProfile/Success.dart';

class Payment extends StatefulWidget {
  const Payment({Key key}) : super(key: key);

  @override
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment> {
  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Color(0xFFa2d5c6),
        title: Text(
          'New payment',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: SizeConfig.height,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(SizeConfig.width * 0.05),
                child: Text(
                  'You can only pay via Credit Card',
                  style: TextStyle(fontSize: SizeConfig.width * 0.05),
                ),
              ),
              CreditCardWidget(
                cardNumber: cardNumber,
                expiryDate: expiryDate,
                cardHolderName: cardHolderName,
                cvvCode: cvvCode,
                showBackView: isCvvFocused,
                // cardbgColor: Colors.black,
                // height: 175,
                textStyle: TextStyle(
                    color: Colors.red,
                    fontSize: SizeConfig.width * 0.04,
                    fontWeight: FontWeight.bold),
                width: MediaQuery.of(context).size.width,
                animationDuration: Duration(milliseconds: 1000),
              ),
              CreditCardForm(
                onCreditCardModelChange: (CreditCardModel creditCardModel) {
                  setState(() {
                    cardNumber = creditCardModel.cardNumber;
                    expiryDate = creditCardModel.expiryDate;
                    cardHolderName = creditCardModel.cardHolderName;
                    cvvCode = creditCardModel.cvvCode;
                    isCvvFocused = creditCardModel.isCvvFocused;
                  });
                },
              ),
              Padding(
                padding: EdgeInsets.all(SizeConfig.width * 0.04),
                child: TextFormField(
                  decoration: InputDecoration(
                    labelText: "Amount of payment",
                    fillColor: Colors.white,
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(),
                    ),
                    //fillColor: Colors.green
                  ),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Amount cannot be empty";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.numberWithOptions(),
                  style: new TextStyle(
                    fontFamily: "Poppins",
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => Success(index: 1)));
                },
                child: Container(
                  height: SizeConfig.height * 0.05,
                  width: SizeConfig.width * 0.9,
                  decoration: BoxDecoration(
                      color: Color(0xFFe52165),
                      borderRadius: BorderRadius.circular(10)),
                  child: Center(
                    child: Text(
                      'Pay',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig.width * 0.045),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
