import 'package:connectivity/connectivity.dart';
import 'package:expandable_bottom_bar/expandable_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geolocator/geolocator.dart' as geolocator;
import 'package:latlng/latlng.dart';
import 'package:permission_handler/permission_handler.dart'
    as PermissionHandler;
import 'package:trio_gym/Configuration/colorConfig.dart';
import 'package:trio_gym/Configuration/sizeConfig.dart';
import 'package:trio_gym/View/Components/RoundedButtonLogin.dart';

import 'Home/HomeScreen.dart';

class SelectMyCountry extends StatefulWidget {
  @override
  _SelectMyCountryState createState() => _SelectMyCountryState();
}

class _SelectMyCountryState extends State<SelectMyCountry> {
  int introIndex = 0;
  LocationPermission permission;
  bool loading = false;
  static bool connected = false;
  bool isLocationEnabled;
  static double lat;
  static double lng;
  bool locationPermission = false;
  static LatLng _initialPosition;
  String locationName = 'Use my location';
  String countryName;
  bool locationDone = false;

  Future<void> _getUserLocation() async {
    loading = true;
    print('_getUserLocation');
    try {
      print('_getUserLocation');
      permission = await Geolocator.checkPermission().whenComplete(() async {
        while (loading) {
          isLocationEnabled = await Geolocator.isLocationServiceEnabled().then(
            (value) async {
              print('_getUserLocation1');
              print(
                  'isLocationServiceEnabled ${Geolocator.isLocationServiceEnabled}');
              Position position = await Geolocator.getCurrentPosition(
                  desiredAccuracy: geolocator.LocationAccuracy.high);
              print('_getUserLocation2');
              setState(() {
                print('_getUserLocation2');
                _initialPosition =
                    LatLng(position.latitude, position.longitude);
                lat = _initialPosition.latitude;
                lng = _initialPosition.longitude;
                locationPermission = true;
              });
              getLocationName();
              locationDone = true;
              return true;
            },
          );
        }
      });
    } catch (e) {
      setState(() {
        locationPermission = false;
        checkGPSPermission();
        loading = false;
        return true;
      });
    }
  }

  void getLocationName() async {
    final coordinates = new Coordinates(lat, lng);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    print('getLocationName');
    var first = addresses.first;
    setState(() {
      loading = false;
    });
    setState(() {
      countryName = first.countryName;
      locationName = '${first.countryName} - ${first.adminArea}';
    });
    print("${first.countryName} : ${first.adminArea}");
  }

  Future<bool> checkInternet() async {
    await Future.delayed(Duration(seconds: 1));
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      setState(() {
        connected = true;
      });
    } else if (connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        connected = true;
      });
    } else {
      setState(() {
        connected = false;
      });
    }
    debugPrint('Am i connected ? $connected');
    return connected;
  }

  Future<void> checkGPSPermission() async {
    print('the checkGPSPermission');
    // if (!await PermissionHandler.Permission.location.isGranted) {
    PermissionHandler.Permission.location.request();
    // }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      extendBody: true,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Select country',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Column(
        children: [
          SizedBox(height: SizeConfig.height * 0.1),
          Center(
            child: Image.asset(
              'assets/iconfinder_pen_stroke_sketch_doodle_lineart_212_451494.png',
              fit: BoxFit.contain,
              height: SizeConfig.height * 0.25,
              width: SizeConfig.height * 0.25,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.width * 0.15,
                vertical: SizeConfig.width * 0.15),
            child: RoundedButtonLogin(
                borderColor: ColorsConfig.primaryTealColor,
                color: ColorsConfig.primaryTealColor,
                title: loading
                    ? CircularProgressIndicator(
                        valueColor:
                            new AlwaysStoppedAnimation<Color>(Colors.white))
                    : Text(
                        locationName,
                        style: TextStyle(
                            fontSize: SizeConfig.width * 0.04,
                            color: Colors.black),
                      ),
                onTap: () async {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) =>
                        DefaultBottomBarController(
                      child: HomeScreen(),
                    ),
                  ));
                  // if (locationDone) {
                  //
                  // } else {
                  //   await checkInternet().whenComplete(() async {
                  //     if (connected == true) {
                  //       await _getUserLocation();
                  //       if (locationPermission == true) {}
                  //     }
                  //   });
                  // }
                }),
          )
        ],
      ),
    );
  }
}
