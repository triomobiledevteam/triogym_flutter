import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';

import '../SelectCountry.dart';

class OnBoardingScreen extends StatefulWidget {
  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        // appBar: AppBar(
        //   title: Text(widget.title),
        // ),
        body: IntroductionScreen(
          pages: [
            PageViewModel(
              decoration: PageDecoration(pageColor: Colors.white),
              titleWidget: Text(
                "Browse",
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
              ),
              bodyWidget: Center(
                child: Text(
                  "Browse and navigate all gyms",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.grey.shade500, fontSize: 20),
                ),
              ),
              image: Image.asset(
                "assets/search.gif",
                width: 150,
              ),
            ),
            PageViewModel(
                decoration: PageDecoration(pageColor: Colors.white),
                titleWidget: Text(
                  "Save Time",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
                bodyWidget: Center(
                  child: Text(
                    'Select gym, offer or period in one place',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.grey.shade500, fontSize: 20),
                  ),
                ),
                image: Image.asset(
                  "assets/clock.gif",
                  width: 150,
                )),
            PageViewModel(
                decoration: PageDecoration(pageColor: Colors.white),
                titleWidget: Text(
                  "Save money",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
                bodyWidget: Center(
                  child: Text(
                    "See and compare all prices, select the best",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.grey.shade500, fontSize: 20),
                  ),
                ),
                image: Image.asset("assets/money.gif")),
            PageViewModel(
                decoration: PageDecoration(pageColor: Colors.white),
                titleWidget: Text(
                  "Manage subscription",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
                bodyWidget: Center(
                  child: Text(
                    "Monitor your subscriptions and rate gyms",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.grey.shade500, fontSize: 20),
                  ),
                ),
                image: Image.asset("assets/gym.gif")),
            PageViewModel(
                decoration: PageDecoration(pageColor: Colors.white),
                titleWidget: Text(
                  "Get offers",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
                bodyWidget: Center(
                  child: Text(
                    "Check all offers in one place",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.grey.shade500, fontSize: 20),
                  ),
                ),
                image: Image.asset("assets/discount.gif"))
          ],
          onDone: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => SelectMyCountry()));
          },
          showSkipButton: true,
          showNextButton: true,
          nextFlex: 1,
          dotsFlex: 2,
          skipFlex: 1,
          animationDuration: 1000,
          curve: Curves.fastOutSlowIn,
          dotsDecorator: DotsDecorator(
              spacing: EdgeInsets.all(5),
              activeColor: Color(0xff20D5B2),
              // activeSize: Size.square(10),
              // size: Size.square(5),
              activeSize: Size(20, 10),
              size: Size.square(10),
              activeShape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25))),
          skip: Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
                color: Color(0xff20D5B2),
                borderRadius: BorderRadius.circular(40),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.shade300,
                      blurRadius: 10,
                      offset: Offset(4, 4))
                ]),
            child: Center(
              child: Text(
                "Skip",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          next: Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40),
                border: Border.all(color: Color(0xff20D5B2), width: 2)),
            child: Center(
              child: Icon(
                Icons.navigate_next,
                size: 30,
                color: Color(0xff20D5B2),
              ),
            ),
          ),
          done: Container(
            height: 60,
            width: 60,
            decoration: BoxDecoration(
                color: Color(0xff20D5B2),
                borderRadius: BorderRadius.circular(40),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.shade300,
                      blurRadius: 40,
                      offset: Offset(4, 4))
                ]),
            child: Center(
              child: Text(
                "Done",
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ));
  }
}
