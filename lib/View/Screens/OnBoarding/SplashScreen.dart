import 'dart:async';

import 'package:flutter/material.dart';
import 'package:trio_gym/View/Screens/OnBoarding/OnBoardingScreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 3),
        () => Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => OnBoardingScreen())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: 180),
          Center(
            child: Image.asset('assets/trioGym.png'),
          ),
          SizedBox(height: 10),
          Text('navigate . select . subscribe'),
          Spacer(),
          Text('Version 1.0.0 | Powered by Triosuite'),
          SizedBox(height: 80),
        ],
      ),
    );
  }
}
