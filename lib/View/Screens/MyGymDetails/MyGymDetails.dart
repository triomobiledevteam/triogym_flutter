import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'package:trio_gym/Configuration/sizeConfig.dart';
import 'package:trio_gym/View/Screens/GymProfile/Edit.dart';
import 'package:trio_gym/View/Screens/GymProfile/PaymentsDetails.dart';

import '../../configuration.dart';

class MyGymDetails extends StatefulWidget {
  @override
  _MyGymDetailsState createState() => _MyGymDetailsState();
}

class _MyGymDetailsState extends State<MyGymDetails> {
  int pageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(backgroundColor: Colors.transparent, elevation: 0),
        body: Container(
          height: SizeConfig.height,
          width: SizeConfig.width,
          child: Stack(
            children: [
              Column(
                children: [
                  Container(
                    height: SizeConfig.height * 0.3,
                    width: SizeConfig.width,
                    decoration: BoxDecoration(
                      color: Color(0xFFe52165),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0),
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Spacer(),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 30.0, horizontal: 20.0),
                          child: Text(
                            'Gym example name\n  The information show here',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 24.0,
                            ),
                            textAlign: TextAlign.start,
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: SizeConfig.height * 0.035),
                    child: Expanded(
                      child: Container(
                        height: SizeConfig.height * 0.65,
                        child: pageIndex == 0
                            ? Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: SizeConfig.width * 0.05),
                                child: Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.width * 0.45,
                                            child: Text(
                                              'Subscription date',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            width: SizeConfig.width * 0.4,
                                            child: Text(
                                              ': 18-05-2021',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.02),
                                      Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.width * 0.45,
                                            child: Text(
                                              'Start date',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            width: SizeConfig.width * 0.4,
                                            child: Text(
                                              ': 18-05-2021',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.02),
                                      Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.width * 0.45,
                                            child: Text(
                                              'End date',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            width: SizeConfig.width * 0.4,
                                            child: Text(
                                              ': 18-05-2021',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.02),
                                      Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.width * 0.45,
                                            child: Text(
                                              'Location',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            width: SizeConfig.width * 0.4,
                                            child: Text(
                                              ': Amman - Jordan',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.02),
                                      Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.width * 0.45,
                                            child: Text(
                                              'Subscription details',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            width: SizeConfig.width * 0.4,
                                            child: Text(
                                              ': Full with training',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.02),
                                      Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.width * 0.45,
                                            child: Text(
                                              'Subscription type',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            width: SizeConfig.width * 0.4,
                                            child: Text(
                                              ': 6 months',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.02),
                                      Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.width * 0.45,
                                            child: Text(
                                              'Payment term',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            width: SizeConfig.width * 0.4,
                                            child: Text(
                                              ': 30 days',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.02),
                                      Row(
                                        children: [
                                          Container(
                                            width: SizeConfig.width * 0.45,
                                            child: Text(
                                              'Payment term',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            width: SizeConfig.width * 0.4,
                                            child: Text(
                                              ': 30 days',
                                              style: TextStyle(
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.02),
                                      InkWell(
                                        onTap: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          EditSubscription()));
                                        },
                                        child: Container(
                                          height: SizeConfig.height * 0.05,
                                          width: SizeConfig.width * 0.5,
                                          decoration: BoxDecoration(
                                              color: Color(0xFFe52165),
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Center(
                                            child: Text(
                                              'Edit',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize:
                                                      SizeConfig.width * 0.045),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Spacer(),
                                      Container(
                                        child: Text(
                                          'Payments',
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize:
                                                  SizeConfig.width * 0.045),
                                        ),
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.02),
                                      Container(
                                        height: SizeConfig.height * 0.07,
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Container(
                                              width: SizeConfig.width * 0.25,
                                              child: Column(
                                                children: [
                                                  Text(
                                                    'Paid',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize:
                                                            SizeConfig.width *
                                                                0.045),
                                                  ),
                                                  Text(
                                                    '300',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize:
                                                            SizeConfig.width *
                                                                0.045),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            VerticalDivider(thickness: 2),
                                            Container(
                                              width: SizeConfig.width * 0.25,
                                              child: Column(
                                                children: [
                                                  Text(
                                                    'Remaining',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize:
                                                            SizeConfig.width *
                                                                0.045),
                                                  ),
                                                  Text(
                                                    '150',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize:
                                                            SizeConfig.width *
                                                                0.045),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            VerticalDivider(thickness: 2),
                                            InkWell(
                                              onTap: () {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (BuildContext
                                                                context) =>
                                                            PaymentDetailsScreen()));
                                              },
                                              child: Container(
                                                width: SizeConfig.width * 0.25,
                                                decoration: BoxDecoration(
                                                    color: Color(0xFFa2d5c6),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                child: Center(
                                                  child: Text(
                                                    'Pay',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize:
                                                            SizeConfig.width *
                                                                0.045),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                          height: SizeConfig.height * 0.05),
                                    ],
                                  ),
                                ),
                              )
                            : MediaQuery.removePadding(
                                context: context,
                                removeTop: true,
                                child: ListView.builder(
                                  // physics: BouncingScrollPhysics(),
                                  itemCount: round1.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return ListTile(
                                      isThreeLine: true,
                                      leading: Container(
                                        width: 90.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                            image: AssetImage(
                                                round1[index]['leading'][0]),
                                            fit: BoxFit.cover,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                      ),
                                      title: Text(round1[index]['title']),
                                      subtitle: Text(
                                          '${round1[index]['subtitle']}\n${round1[index]['leading'][1]}'),
                                      trailing: round1[index]['trailing'],
                                    );
                                  },
                                ),
                              ),
                      ),
                    ),
                  )
                ],
              ),
              Positioned(
                top: SizeConfig.height * 0.27,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.width * 0.045),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ToggleSwitch(
                        initialLabelIndex: pageIndex,
                        minHeight: SizeConfig.height * 0.05,
                        minWidth: SizeConfig.width * 0.7,
                        activeBgColor: Color(0xFFa2d5c6),
                        activeFgColor: Colors.black,
                        inactiveFgColor: Colors.white,
                        inactiveBgColor: Colors.grey,
                        labels: ['My subscription', 'Gym info'],
                        onToggle: (index) {
                          print('switched to: $index');
                          setState(() {
                            pageIndex = index;
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
