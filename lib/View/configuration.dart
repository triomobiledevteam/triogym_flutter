import 'package:flutter/material.dart';

final List<Map<String, dynamic>> round1 = [
  {
    'leading': ['assets/gym/gym1.jpeg', '10 x'],
    'title': 'Single-leg deadlifts',
    'subtitle': '00:30',
    'trailing': Icon(Icons.chevron_right, size: 25),
  },
  {
    'leading': ['assets/gym/gym2.jpeg', '15 x'],
    'title': 'Dumbbell rolls',
    'subtitle': '00:45',
    'trailing': Icon(Icons.chevron_right, size: 25),
  },
  {
    'leading': ['assets/gym/gym3.png', '1 x'],
    'title': 'Side plank',
    'subtitle': '01:00',
    'trailing': Icon(Icons.chevron_right, size: 25),
  },
  {
    'leading': ['assets/gym/gym4.jpeg', '12 x'],
    'title': 'Glute bridge',
    'subtitle': '00:50',
    'trailing': Icon(Icons.chevron_right, size: 25),
  },
  {
    'leading': ['assets/gym/gym5.png', '1 x'],
    'title': 'Side plank',
    'subtitle': '01:00',
    'trailing': Icon(Icons.chevron_right, size: 25),
  },
  {
    'leading': ['assets/gym/gym6.jpeg', '12 x'],
    'title': 'Glute bridge',
    'subtitle': '00:50',
    'trailing': Icon(Icons.chevron_right, size: 25),
  },
  {
    'leading': ['assets/gym/gym6.jpeg', '12 x'],
    'title': 'Glute bridge',
    'subtitle': '00:50',
    'trailing': Icon(Icons.chevron_right, size: 25),
  },
  {
    'leading': ['assets/gym/gym6.jpeg', '12 x'],
    'title': 'Glute bridge',
    'subtitle': '00:50',
    'trailing': Icon(Icons.chevron_right, size: 25),
  }
];
