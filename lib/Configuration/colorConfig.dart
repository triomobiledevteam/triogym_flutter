import 'package:flutter/cupertino.dart';

class ColorsConfig {
  static Color primaryGreenColor = Color(0xFF589875);
  static Color primaryOrangeColor = Color(0xFFEC7863);
  static Color primaryTealColor = Color(0xff20D5B2);

  static Color secondaryDarkGreenColor = Color(0xFF3C6950);
  static Color secondaryMedGreenColor = Color(0xFF84C29D);
  static Color secondaryLightGreenColor = Color(0xFFD7E9DB);
  static Color secondaryDarkOrangeColor = Color(0xFFAA5849);
  static Color secondaryMedOrangeColor = Color(0xFFEDA694);
  static Color secondaryLightOrangeColor = Color(0xFFFDE1D6);

  static Color greyDivider = Color(0xFFF0F3F6);
  static Color greyBackground = Color(0xFFF0F0F0);
  static Color greyProgress = Color(0xFFC6CBD0);

  static Color greyColorDark1 = Color(0xFF303234);
  static Color greyColorDark2 = Color(0xFF898989);
  static Color greyColorDark3 = Color(0xFFC1C1C1);
  static Color greySplashText = Color(0xFF9C9FA2);

  static Color greyColorLight1 = Color(0xFFEBEBEB);
  static Color greyColorLight2 = Color(0xFFF7F7F7);
  static Color greyColorLight3 = Color(0xFFFFFFF);

  static Color boxYellowContent = Color(0xFFFFECCC);
  static Color boxYellowBorder = Color(0xFFDEAC5B);
  static Color statusColorYellow = Color(0xFFFFC000);
  static Color statusColorGreen = Color(0xFF70B942);
  static Color statusColorRed = Color(0xFFE03F3F);

  static LinearGradient orangeGradient =
      LinearGradient(colors: [Color(0xFFEC7863), Color(0xFFAA5849)]);
  static LinearGradient greenGradient =
      LinearGradient(colors: [Color(0xFF7AAC8F), Color(0xFF49715A)]);
  static LinearGradient greyGradient =
      LinearGradient(colors: [Color(0xFFC7CBD0), Color(0xFF89909A)]);
  static LinearGradient blackGradient =
      LinearGradient(colors: [Color(0xFF9A9A9A), Color(0xFF4B4B4B)]);
}
