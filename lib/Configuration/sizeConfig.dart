import 'package:flutter/widgets.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double width;
  static double height;
  static double inButtonTxtPortrait;
  static double inButtonTxtLandscape;
  static double inTxtFieldPortrait;
  static double inTxtFieldLandscape;
  static double fullPadding;
  static double sizeBoxH;
  static double sizeBoxW;
  static double mainScreenSmallTxt;
  static double mainScreenVerySmallTxt;
  static double mainScreenLargTxt;
  static double containerHalfSize;
  static double horizontalP;
  static double verticalP;
  static double blockSizeHorizontal;
  static double blockSizeVertical;
  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    width = _mediaQueryData.size.width;
    height = _mediaQueryData.size.height;
    horizontalP = width * 0.05;
    verticalP = height * 0.1;
    mainScreenSmallTxt = width * 0.05;
    mainScreenLargTxt = width * 0.1;
    mainScreenVerySmallTxt = width * 0.03;
    inTxtFieldPortrait = width * 0.04;
    inTxtFieldLandscape = width * 0.02;
    containerHalfSize = width / 2 - width * 0.15;
    blockSizeHorizontal = width / 100;
    blockSizeVertical = height / 100;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (width - _safeAreaHorizontal) / 100;
    safeBlockVertical = (height - _safeAreaVertical) / 100;
  }
}
